### WORKHELPER CLI

Commands:

- say-hello
  > workhelper say-hello
- create-component
  > workhelper create-component

Command has 2 flags: `--javascript` or `-j` and `--scss` or `-s`.

UPDATES:
v0.0.4 - Добавлены дефолтные данные пути для создания компонента и добавлена поддержка path аргумента
