import fs from "fs-extra";
import filterRow from "../../lib/filterRow";
import mkFileSync from "../../lib/mkFileSync";

async function createReactComponent(options) {
  const { isJavascript, isScss, isInnerStyled, name, path } = options;

  const folderPath = `${path}/${name}`;
  const extension = isJavascript ? "js" : "ts";

  const paths = {
    folder: `${folderPath}`,
    scss: `${folderPath}/${name}Styles.scss`,

    index: `${folderPath}/index.${extension}`,
    component: `${folderPath}/${name}.${extension}x`,
    styled: `${folderPath}/${name}Styled.${extension}`,
    interface: `${folderPath}/${name}Types.${extension}`,
  };

  const indexFileRows = [
    `import ${name} from './${name}';`,
    `export default ${name};`,
  ];

  const styledFileRows = [
    `import styled from 'styled-components';`,
    !isJavascript &&
      `import { ${name}Props } from './${name}Types'`,
    ``,
    !isJavascript &&
      `export const Styled${name} = styled.div<${name}Props>${"``"};`,
    isJavascript && `export const Styled${name} = styled.div${"``"};`,
  ].filter(filterRow);

  const scssFileRows = [`.${name} { };`];

  const interfaceFileRows = [`export interface ${name}Props {}`];

  const componentFileRows = [
    `import React from 'react';`,
    (!isScss && isInnerStyled) && `import styled from 'styled-components';`,
    !isJavascript &&
      `import { ${name}Props } from './${name}Types';`,
    (!isScss && !isInnerStyled) && `import { Styled${name} } from './${name}Styled';`,
    isScss && `import './${name}Styles.scss';`,
    ``,

    (!isScss && isInnerStyled) && `const Styled${name} = styled.div<${name}Props>${"``"};`,
    (!isScss && isInnerStyled) && ``,

    !isJavascript && `const ${name} = (props: ${name}Props) => {`,
    isJavascript && `const ${name} = (props) => {`,
    !isScss && `  return <Styled${name}></Styled${name}>;`,
    isScss && `  return <div class="${name}"></div>`,
    `};`,
    ``,
    `export default ${name};`,
  ].filter(filterRow);

  if (!fs.existsSync(paths.folder)) {
    await fs.mkdirSync(paths.folder);
    await mkFileSync(paths.index, indexFileRows);
    await mkFileSync(paths.component, componentFileRows);

    if (!isJavascript) await mkFileSync(paths.interface, interfaceFileRows);

    if (isScss) await mkFileSync(paths.scss, scssFileRows);
    else if(!isInnerStyled)await mkFileSync(paths.styled, styledFileRows);
  } else {
    throw Error(`${name} folder has already been created`);
  }
}

export default createReactComponent;
