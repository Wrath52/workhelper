import arg from "arg";
import fs from "fs-extra";

function createComponentArguments(argv) {
  const args = arg(
    {
      "--javascript": Boolean,
      "-j": "--javascript",
      "--scss": Boolean,
      "-s": "--scss",
      "--innerStyled": Boolean,
      "-i": "--innerStyled",
      "--path": String,
      "-p": "--path",
    },
    { argv }
  );

  const processPath = process.cwd();

  const defaultPath = (() => {
    if (fs.existsSync(`${processPath}/src`)) {
      if (fs.existsSync(`${processPath}/src/components`)) {
        return `${processPath}/src/components`;
      }
    }

    if (fs.existsSync(`${processPath}/source`)) {
      if (fs.existsSync(`${processPath}/source/components`)) {
        return `${processPath}/source/components`;
      }
    }

    return undefined;
  })();

  const argumentsPath = (() => {
    if (args["--path"]) {
      if (args["--path"][0] === "/") {
        return args["--path"];
      }

      if (args["--path"].slice(0, 2) === "./") {
        return `${processPath}/${args["--path"].replace("./", "")}`;
      }

      return `${processPath}/${args["--path"]}`;
    }
  })();

  return {
    name: args._[1],
    path: argumentsPath || defaultPath || processPath,

    isScss: args["--scss"] || false,
    isJavascript: args["--javascript"] || false,
    isInnerStyled: args["--innerStyled"] || false,
  };
}

export default createComponentArguments;
