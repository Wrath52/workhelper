import chalk from "chalk";

const messages = {
  done: "Done, happy coding :)",
  understand: "workhelper don't know this command",
}

class Messenger {
  done() {
    console.log(chalk.green(messages.done));
  }

  understand() {
    console.log(chalk.red(messages.understand));
  }

  error(message) {
    console.log(chalk.red(message));
  }
}

export default new Messenger();