import createReactComponent from "./commands/createCommand/createComponent";
import createComponentArguments from "./commands/createCommand/createComponentArguments";

import sayHello from "./commands/sayHello/sayHello";
import sayHelloArguments from "./commands/createCommand/createComponentArguments";

import Messenger from "./utils/Messenger";

export async function cli(args) {
  const [command] = args;

  try {
    switch (command) {
      case "create-component":
        await createReactComponent(createComponentArguments(args));
        Messenger.done();
        break;

      case "say-hello":
        await sayHello(sayHelloArguments(args));
        break;

      default:
        Messenger.understand();
        break;
    }
  } catch (error) {
    Messenger.error(error.message)
    process.exit(0);
  }
}
