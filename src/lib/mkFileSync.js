import fs from "fs-extra";

const mkFileSync = async (path, content) => {
  const writeProcess = await fs.openSync(path, "w");

  for (let i = 0; i < content.length; i++) {
    const row = content[i];
    const isLast = i === content.length - 1;

    const rowEnd = isLast ? "" : "\n";

    await fs.writeSync(writeProcess, row + rowEnd);
  }

  await fs.closeSync(writeProcess);
};


export default mkFileSync