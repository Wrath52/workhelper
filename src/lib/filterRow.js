const filterRow = (a) => a === "" || Boolean(a);

export default filterRow